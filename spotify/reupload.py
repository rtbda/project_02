import json
import sys
from warnings import catch_warnings
from elasticsearch import Elasticsearch


CLOUD_ID = "";
ELASTIC_PASSWORD = "";
INDEX_NAME ="top_50_songs_worldwide"

def main(filepath):
    client = Elasticsearch(cloud_id=CLOUD_ID, basic_auth=("elastic", ELASTIC_PASSWORD));
    responses = [];
    song_objects = []
    try:
        with open(filepath, 'r') as f:
            print(filepath)
            for line in f:
                song_objects.append(json.loads(line.strip()))
            f.close();
    except:
        print("File not found")
        exit()
    
    t = filepath.split("/")[-1].replace(".json", "").replace("_", "/")
    for i, s in enumerate(song_objects):
        doc_id = t + "_" + str(i + 1);
        resp = client.index(index=INDEX_NAME, id=doc_id, document=s);
        responses.append(resp['result']);
    
    positive = responses.count("created");
    print("Created entries: " + str(positive) + "/50");
    





if __name__ == '__main__':
    if not len(sys.argv) == 2:
        print("Incorrect number of args. Enter file name.")
        exit();
    main(sys.argv[1]);

