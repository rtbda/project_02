import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import json
import requests
from datetime import date
from elasticsearch import Elasticsearch


CLIENT_ID = ''
CLIENT_SECRET = ''
PLAYLIST_ID = "37i9dQZEVXbMDoHDwVN2tF" # Global top 50;
CLOUD_ID = "";
ELASTIC_PASSWORD = "";
INDEX_NAME ="top_50_songs_worldwide"

def main():
    # Initialize the clients for spotify and elasticsearch
    client_credentials_manager = SpotifyClientCredentials(client_id=CLIENT_ID, client_secret=CLIENT_SECRET);
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager);
    client = Elasticsearch(cloud_id=CLOUD_ID, basic_auth=("elastic", ELASTIC_PASSWORD));
    ## print(client.info());
    artist_name = [];
    track_name = [];
    track_popularity = [];
    artist_id = [];
    track_id = [];
    track_genres = [];
    danceability = [];
    energy = [];
    key = [];
    loudness = [];
    mode = [];
    speechiness = [];
    acousticness = [];
    instrumentalness = [];
    liveness = [];
    valence = [];
    tempo = [];
    duration_ms = [];
    time_signature = [];

    # Query playlist
    query_result = sp.playlist(PLAYLIST_ID);
    tracks = query_result["tracks"]["items"];

    ## Extract the API data.
    for t in tracks:
        taf = (sp.audio_features(t['track']['id']))[0];
        artist_name.append(t['track']['artists'][0]['name']);
        artist_id.append(t['track']['artists'][0]['id']);
        artist = sp.artist((t['track']['artists'][0]['id']));
        track_genres.append(artist['genres']);
        track_name.append(t['track']['name']);
        track_id.append(t['track']['id']);
        track_popularity.append(t['track']['popularity']);
        danceability.append(taf["danceability"])
        energy.append(taf["energy"])
        key.append(taf["key"])
        loudness.append(taf["loudness"])
        mode.append(taf["mode"])
        speechiness.append(taf["speechiness"])
        acousticness.append(taf["acousticness"])
        instrumentalness.append(taf["instrumentalness"])
        liveness.append(taf["liveness"])
        valence.append(taf["valence"])
        tempo.append(taf["tempo"])
        duration_ms.append(taf["duration_ms"])
        time_signature.append(taf["time_signature"])
    

    song_objects = []
    t = date.today().strftime("%m/%d/%Y");
    today = date.today().isoformat();
    print(t)
    
    ## Create the JSON object.
    for i in range(50):
        x = json.dumps({
            "timestamp": today,
            "ranking": i + 1,
            "artist_name": artist_name[i],
            "track_name": track_name[i],
            "track_popularity": track_popularity[i],
            "artist_id": artist_id[i],
            "track_id": track_id[i],
            "track_genres": track_genres[i],
            'danceability': danceability[i],
            'energy': energy[i],
            'key': key[i],
            'loudness': loudness[i],
            'mode': mode[i],
            'speechiness': speechiness[i],
            'acousticness': acousticness[i],
            'instrumentalness': instrumentalness[i],
            'liveness': liveness[i],
            'valence': valence[i],
            'tempo': tempo[i],
            'duration_ms': duration_ms[i],
            'time_signature': time_signature[i],
        });
        song_objects.append(x);
    

    # Upload the json objects to elasticsearch/kibana.
    responses = [];
    for i, s in enumerate(song_objects):
        doc_id = t + "_" + str(i + 1);
        resp = client.index(index=INDEX_NAME, id=doc_id, document=s);
        responses.append(resp['result']);
    
    positive = responses.count("created");
    print("Created entries: " + str(positive) + "/50");

    filepath = "./lists/" + t.replace('/', '_') + ".json"
    try:
        with open(filepath, 'a') as f:
            f.writelines('\n'.join(song_objects));
            f.close();
    except e:
        print(e)



if __name__ == '__main__':
    main();