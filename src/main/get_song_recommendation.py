import json
import requests
from datetime import date
from elasticsearch import Elasticsearch
import numpy as np
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials


CLIENT_ID = ''
CLIENT_SECRET = ''

CLOUD_ID = "";
ELASTIC_PASSWORD = "";
INDEX_NAME ="top_50_songs_worldwide"

HASTHAGS = ["#war", "#music", "#health"];
INDEX_NAME_SONG ="top_50_songs_worldwide";
INDEX_NAME_SENTIMENT ="sentiment";


def main():
    t = date.today().strftime("%Y-%m-%d");
    client_credentials_manager = SpotifyClientCredentials(client_id=CLIENT_ID, client_secret=CLIENT_SECRET);
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager);
    client = Elasticsearch(cloud_id=CLOUD_ID, basic_auth=("elastic", ELASTIC_PASSWORD));
    r = dict(zip(HASTHAGS , [None] * len(HASTHAGS)));
    def get_songs():
        s = []
        a =  client.search(index=INDEX_NAME_SONG, query={
            "match": {
                "timestamp": t
            },
        }, size=50
        )

        for b in a.body['hits']['hits']:
            s.append(b['_source'])
        return s;

    def get_sentiment(hasthag):
        query = {
  "aggs": {
    "0-bucket": {
      "filter": {
        "bool": {
          "must": [],
          "filter": [
            {
              "bool": {
                "should": [
                  {
                    "match_phrase": {
                      "hashtag": hasthag
                    }
                  }
                ],
                "minimum_should_match": 1
              }
            }
          ],
          "should": [],
          "must_not": []
        }
      },
      "aggs": {
        "0-metric": {
          "avg": {
            "field": "sentiment"
          }
        }
      }
    }
  },
  "size": 0,
  "fields": [
    {
      "field": "timestamp",
      "format": "date_time"
    }
  ],
  "script_fields": {},
  "stored_fields": [
    "*"
  ],
  "runtime_mappings": {},
  "_source": {
    "excludes": []
  },
  "query": {
    "range": {
        "timestamp": {
            "gte": "now-1d/d",
            "lt": "now/d"
        }
    }
}
}
        response = client.search(index=INDEX_NAME_SENTIMENT, body=query)
        return response.body['aggregations']['0-bucket']['0-metric']['value']

    songs = get_songs();
    songs_valence = np.zeros((len(songs)));
    for idx, s in enumerate(songs):
        songs_valence[idx]= s['valence']

    songs_valence = np.tile(songs_valence, (len(HASTHAGS),1));
    for idx, key in enumerate(r):
        s = get_sentiment(key);
        if not s:
            continue;
        r[key] = s
        songs_valence[idx] = np.abs(songs_valence[idx] - s);


    smallest_difference_index = np.zeros(len(HASTHAGS));
    recommendations = [];
    for x in range(len(HASTHAGS)):
        idx = songs_valence[x].argmin();
        smallest_difference_index[x] = idx
        recommendations.append(songs[idx]);


    print("Song recommendation:");
    for idx, key in enumerate(r):
        print("For {} with valence: {}".format(key, r[key]));
        print("Song: {}".format(recommendations[idx]));
        try:
            asdf = sp.track(recommendations[idx]['track_id'])['external_urls']['spotify'];
            print("URL: {}". format(asdf));
        except:
            print("No url :(");
        print();




if __name__ == '__main__':
    main();