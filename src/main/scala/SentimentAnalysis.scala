import java.util.{Properties, Collections}
import org.apache.kafka.clients.consumer.{KafkaConsumer}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.PipelineModel
import org.apache.spark.sql.functions._
import org.apache.commons.io.FileUtils
import java.io.File
import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization;



case class IncomingMessageTweet(tweet: String, timestamp: String);

object SentimentAnalysis {
  def main (args: Array[String]):Unit={
    Logger.getLogger("org").setLevel(Level.OFF)
      val topicName="tweets"
      val topicOutputName="sentiment"

    val spark = SparkSession.builder()
      .master("local[1]")
      .appName("SentimentAnalysis")
      .getOrCreate()
    import spark.implicits._

    val sc=spark.sparkContext
    implicit val formats = DefaultFormats


    // Loading trained model model
    val pipelineFitPath = "models/pipelineModel"
    val pipelineLRPath = "models/lrModel"
    val pipelineFit = PipelineModel.load(pipelineFitPath);
    val lrModel = LogisticRegressionModel.load(pipelineLRPath);


    // Set up for Kafka
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:29092")
    props.put("group.id", "aaaaaaa")
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    
    props.put("acks", "all")
    props.put("retries", "0")
    props.put("batch.size", "16384");
    props.put("buffer.memory", "33554432");
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    val consumer = new KafkaConsumer[String, String](props)
    val producer = new KafkaProducer[String, String](props)

    try {
      consumer.subscribe(Collections.singletonList(topicName));
      println("Sentiment analysis listening to tweet topic")
      while (true){
        val records = consumer.poll(10)
        records.forEach(r => {
          val hasthag = r.key();
          val c = r.value();
          try {
            val d = Serialization.read[IncomingMessageTweet](c);
            val data = Seq(d.tweet).toDF("sentence");
            val pm = pipelineFit.transform(data);
            val result = lrModel.transform(pm);
            val prediction = (1- result.select("prediction").first().getDouble(0).toInt).toString();
            val content = """{"tweet":"%s", "timestamp":"%s", "hashtag": "%s", "sentiment": "%s"} """.format(d.tweet, d.timestamp, hasthag, prediction);
            val record = new ProducerRecord(topicOutputName, "key", content);
            producer.send(record).get()
            println(content); 
          } catch {
            case e: Exception => e.printStackTrace()
          }
        })
      }

    } catch {
      case e: Exception => e.printStackTrace()

    } finally {
      consumer.close()
    }
  }
}