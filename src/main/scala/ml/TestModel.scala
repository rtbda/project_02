import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer, CountVectorizer, StringIndexer }
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.Pipeline
import scala.math.pow;
import org.apache.spark.sql.types.{StructType, StringType, IntegerType}
import org.apache.spark.ml.PipelineModel
import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.spark.sql.Column


object TestModel {
  def main (args: Array[String]):Unit={
    Logger.getLogger("org").setLevel(Level.OFF)

    val spark = SparkSession.builder()
      .master("local[2]")
      .appName("Training")
      .getOrCreate()
    val sc=spark.sparkContext
      import spark.implicits._;
      val seed = 42;

    val schema = new StructType()
    .add("res", IntegerType, true)
    .add("sentence", StringType, true)

    val pipelineFitPath = "models/pipelineModel"
    val pipelineLRPath = "models/lrModel"
    val testDataPath = "data/split/test";
    val pipelineFit = PipelineModel.load(pipelineFitPath);
    val lrModel = LogisticRegressionModel.load(pipelineLRPath);
    val testSet = spark.read.option("header", false).schema(schema).csv(testDataPath);
    val testDF = pipelineFit.transform(testSet);
    val predictions = lrModel.transform(testDF);
    predictions.show();
    val evaluator = new BinaryClassificationEvaluator().setRawPredictionCol("rawPrediction");
    val resEvaluation = evaluator.evaluate(predictions)
    println(resEvaluation);


    val data = Seq("I am happy, that was an amazing night at the club",
    "I am sad. Why did she left me. All is going wrong",
      "Ugh, this sucks. I could really use a break",
      "What an incredible night. The skyline was breathtaking, the food in point. I am in awe with the city" 
      ).toDF("sentence")
    val pm = pipelineFit.transform(data);
    val result = lrModel.transform(pm)
    result.show()
    val data2 = Seq("Good morning!").toDF("sentence")
    val pm2 = pipelineFit.transform(data2);
    val result2 = lrModel.transform(pm2)
    result2.show()
    val prediction = result2.select("prediction").first().getDouble(0)
    println(prediction);
    println("done")
  }
}