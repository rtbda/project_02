import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer, CountVectorizer, StringIndexer }
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.Pipeline
import scala.math.pow;
import org.apache.spark.sql.types.{StructType, StringType, IntegerType}

object Training {
  def main (args: Array[String]):Unit={
    Logger.getLogger("org").setLevel(Level.OFF)

    val spark = SparkSession.builder()
      .master("local[2]")
      .appName("Training")
      .getOrCreate()
    val sc=spark.sparkContext
      import spark.implicits._;
      val seed = 42;

    val schema = new StructType()
    .add("res", IntegerType, true)
    .add("id", StringType, true)
    .add("timestamp", StringType, true)
    .add("topic", StringType, true)
    .add("user", StringType, true)
    .add("sentence", StringType, true)


    
    val trainingDataPath = "data/base/training.1600000.processed.noemoticon.csv";
    println("Reading...");
    val d = spark.read.option("header", false).schema(schema).csv(trainingDataPath);
    val data = d.drop("id", "timestamp", "topic", "user")
    val Array(trainingSet, validationSet, testSet) = data.randomSplit(Array(0.98, 0.01, 0.01), 42);

    trainingSet.write.csv("data/split/training")
    validationSet.write.csv("data/split/validation")
    testSet.write.csv("data/split/test")

    // Tokenizer
    // https://spark.apache.org/docs/latest/ml-features.html#tokenizer
    // Tokenizer breaks the sentence into individual words 
    val tokenizer = new Tokenizer().setInputCol("sentence").setOutputCol("words")
    // HashingTF
    // https://spark.apache.org/docs/latest/ml-features.html#tf-idf
    // Converts the words into fixed-length feature vectors
    val hashingTF = new HashingTF().setInputCol("words").setOutputCol("rawFeatures").setNumFeatures(pow(2,16).toInt)
    // IDF
    // https://spark.apache.org/docs/latest/ml-features.html#tf-idf
    // Fits the dataset
    val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features").setMinDocFreq(5)
    val indexer = new StringIndexer().setInputCol("res").setOutputCol("label")
    
    // Set the pipeline stages
    val pipeline = new Pipeline().setStages(Array(tokenizer, hashingTF, idf, indexer))
   
    println("Fitting...");
    val pipelineFit = pipeline.fit(trainingSet);

    println("Saving model...");
    pipelineFit.write.overwrite().save("models/pipelineModel");
    println("Transforming data...");
    val trainingDF = pipelineFit.transform(trainingSet);
    val validationDF = pipelineFit.transform(validationSet);
    val lr = new LogisticRegression().setMaxIter(100)
    val lrModel = lr.fit(trainingDF);
    lrModel.write.overwrite().save("models/lrModel")
    val predictions = lrModel.transform(validationDF);
    val evaluator = new BinaryClassificationEvaluator().setRawPredictionCol("rawPrediction");
    val resEvaluation = evaluator.evaluate(predictions)
    println(resEvaluation);
    println("done")
  } 
}