import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer, CountVectorizer, StringIndexer }
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.Pipeline
import scala.math.pow;
import org.apache.spark.sql.types.{StructType, StringType, IntegerType}
import org.apache.spark.ml.PipelineModel
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.spark.sql.Column
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics


object StatisticModel {
  def main (args: Array[String]):Unit={
    Logger.getLogger("org").setLevel(Level.OFF)

    val spark = SparkSession.builder()
      .master("local[2]")
      .appName("Training")
      .getOrCreate()
    val sc=spark.sparkContext
      import spark.implicits._;
      val seed = 42;

    val schema = new StructType()
    .add("res", IntegerType, true)
    .add("sentence", StringType, true)

    val pipelineFitPath = "models/pipelineModel"
    val pipelineLRPath = "models/lrModel"
    val testDataPath = "data/split/test";
    val pipelineFit = PipelineModel.load(pipelineFitPath);
    val lrModel = LogisticRegressionModel.load(pipelineLRPath);
    val testSet = spark.read.option("header", false).schema(schema).csv(testDataPath);
    val testDF = pipelineFit.transform(testSet);
    val predictions = lrModel.transform(testDF)

    val a = predictions.map(e => 
      (e.getDouble(5), e.getDouble(8))
    )


    val metrics = new BinaryClassificationMetrics(a.rdd)
 
    println("Precision")
    val precision = metrics.precisionByThreshold
    precision.foreach { case (t, p) =>
      println(s"Threshold: $t, Precision: $p")
    }

    // Recall by threshold
    println("Recall by threshold")
    val recall = metrics.recallByThreshold
    recall.foreach { case (t, r) =>
      println(s"Threshold: $t, Recall: $r")
    }

    // Precision-Recall Curve
    println("Precision-Recall Curve")
    val PRC = metrics.pr

    // F-measure
    println("F-measure")
    val f1Score = metrics.fMeasureByThreshold
    f1Score.foreach { case (t, f) =>
      println(s"Threshold: $t, F-score: $f, Beta = 1")
    }

    val beta = 0.5
    val fScore = metrics.fMeasureByThreshold(beta)
    f1Score.foreach { case (t, f) =>
      println(s"Threshold: $t, F-score: $f, Beta = 0.5")
    }

    // AUPRC
    println("AUPRC")
    val auPRC = metrics.areaUnderPR
    println("Area under precision-recall curve = " + auPRC)

    // Compute thresholds used in ROC and PR curves
    println("Compute thresholds used in ROC and PR curves")
    val thresholds = precision.map(_._1)

    // ROC Curve
    val roc = metrics.roc

    // AUROC
    println("AUROC")
    val auROC = metrics.areaUnderROC
    println("Area under ROC = " + auROC)


  }
}