import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import scala.collection.mutable.ListBuffer
import java.time.LocalDateTime;

case class TweetMessage(tweet: String, timestamp: String);

object TwitterP {
  def main (args: Array[String]):Unit={
    Logger.getLogger("org").setLevel(Level.OFF)
    val topicName="tweets"
    val consumerKey = ""
    val consumerSecret=""
    val accessToken=""
    val accessTokenSecret=""
    System.setProperty("twitter4j.oauth.consumerKey", consumerKey)
    System.setProperty("twitter4j.oauth.consumerSecret", consumerSecret)
    System.setProperty("twitter4j.oauth.accessToken", accessToken)
 System.setProperty("twitter4j.oauth.accessTokenSecret", accessTokenSecret)
      val lang = "en"
      val filter = Array("#health", "#music", "#war")


    val spark = SparkSession.builder()
      .master("local[2]")
      .appName("TwitterP")
      .getOrCreate()

    val sc=spark.sparkContext
    val ssc = new StreamingContext(sc, Seconds(5))
    val tweets = TwitterUtils.createStream(ssc, None, filter)
    val data=tweets.filter(tweet => tweet.getLang()==lang).map(status=>(status.getText(), LocalDateTime.now().toString()))
    val props = new Properties()

    props.put("bootstrap.servers", "localhost:29092")
    props.put("acks", "all")
    props.put("retries", "0")
    props.put("batch.size", "16384");
    props.put("buffer.memory", "33554432");
    
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    println("Consuming from twitter")
    data.foreachRDD(rdd=> {
      rdd.foreachPartition(l => {
        val producer = new KafkaProducer[String, String](props)
        l.foreach { tweet => {
          filter.foreach(ht => {
            if (tweet._1.contains(ht)){
              val content = """{"tweet":"%s", "timestamp":"%s"} """.format(tweet._1, tweet._2);
              val record = new ProducerRecord(topicName, ht, content);
              producer.send(record).get()
              println(content);

          }
          })          
        }
        }
        producer.close()
      }
      )
    })
    ssc.start()
    ssc.awaitTermination()
  }

}