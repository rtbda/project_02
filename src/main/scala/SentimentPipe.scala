import java.util.{Properties, Collections}
import org.apache.kafka.clients.consumer.{KafkaConsumer}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization;
import org.elasticsearch.spark.rdd.EsSpark;



case class IncomingMessageSentiment(tweet: String, timestamp: String, hashtag: String, sentiment: String);
case class ESDocumentSentiment(tweet: String, timestamp: String, hashtag: String, sentiment: Integer);

object SentimentPipe {
  def main (args: Array[String]):Unit={
    Logger.getLogger("org").setLevel(Level.OFF);
    val topicName = "sentiment";
    val indexName = "sentiment";

    
    val spark = SparkSession.builder()
      .master("local[2]")
      .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
      .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
      .config("es.nodes", "")
      .config("es.net.http.auth.user", "")
      .config("es.net.http.auth.pass", "")
      .config("es.nodes.discovery","false")
      .config("spark.es.port","9243")
      .config("es.nodes.wan.only", "true")
      .config("es.index.auto.create", "true")
      .appName("SentimentPipe")
      .getOrCreate()
    import spark.implicits._

    val sc=spark.sparkContext
    implicit val formats = DefaultFormats

    // Set up for Kafka
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:29092")
    props.put("group.id", "aaaaaaa")
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")

    val consumer = new KafkaConsumer[String, String](props)

    try {
      consumer.subscribe(Collections.singletonList(topicName));
      println("Piping sentiments")

      while (true){
        val records = consumer.poll(10)
        records.forEach(r => {
          try {
          val c = r.value();
          val d = Serialization.read[IncomingMessageSentiment](c);
          val doc = ESDocumentSentiment(d.tweet, d.timestamp, d.hashtag, d.sentiment.toInt);
          val rdd = sc.makeRDD(Seq(doc));
          EsSpark.saveToEs(rdd, indexName);
          val content = """{"tweet":"%s", "timestamp":"%s", "hashtag": "%s", "sentiment": "%s"} """.format(d.tweet, d.timestamp, d.hashtag, d.sentiment);
          println(content);
          } catch {
            case e: Exception => e.printStackTrace()
          }
        })
      }

    } catch {
      case e: Exception => e.printStackTrace()

    } finally {
      consumer.close()
    }
  }
}