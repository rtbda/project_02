# Project 02

# Steps.

1. Download the tweet data for the project.
   ```
   mkdir -p data/base data/split
   cd data/base
   wget https://cs.stanford.edu/people/alecmgo/trainingandtestdata.zip
   unzip trainingandtestdata.zip
   ```

2. Run [Training.scala](./src/main/scala/ml/Training.scala) to train the model.
3. (Optional) Run [TestModel.scala](./src/main/scala/ml/TestModel.scala).
4. Get your Twitter dev credentials for an elevated projects from [here](https://developer.twitter.com/)
5. Add your consumerSecret, accessToken, and accessTokenSecret to [TwitterP.scala](./src/main/scala/TwitterP.scala).
6. Create a cloud instance from [elastic](https://www.elastic.co/).
7. Get your node address, user, password, and cloud id from elasticsearch and add them in the config for spark in [SentimentPipe.scala](./src/main/scala/SentimentPipe.scala) and into both [get_song_recommendation.py](./src/main/get_song_recommendation.py) and [spotify/main.py](spotify/main.py).
8. Get your spotify credentials from [developers.spotify.com](https://developer.spotify.com/) and add them to [get_song_recommendation.py](./src/main/get_song_recommendation.py) and [spotify/main.py](spotify/main.py).
9. Set up a cloud function that runs each day the python script [spotify/main.py](./spotify/main.py) or run it manually.

10. Change to the project source directory and Run zookeeper and Kafka with:
    ```
    docker-compose up
    ```
11. Run the files [SentimentPipe.scala](./src/main/scala/SentimentPipe.scala), [TwitterP.scala](./src/main/scala/TwitterP.scala), and [SentimentAnalysis.scala](./src/main/scala/SentimentAnalysis.scala).
12. Enter your cloud instance of elastic search and make visualizations of tweets and the spotify tracks.
