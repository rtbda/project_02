// scalaVersion := "2.11.12"
scalaVersion := "2.12.9"

name := "project02"
version := "1.0"

libraryDependencies ++= Seq(
    "org.apache.spark" %% "spark-core" % "2.4.0",
    "org.apache.spark" %% "spark-sql" % "2.4.0",
    "org.apache.spark" %% "spark-streaming" % "2.4.0",
    "org.apache.bahir" %% "spark-streaming-twitter" % "2.4.0",
    "org.apache.kafka" %% "kafka" % "2.3.0",
    "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.4.0",
    "org.apache.spark" %% "spark-sql-kafka-0-10" % "2.4.0",
    "org.apache.spark" %% "spark-mllib" % "2.4.3" % "provided",
    "com.johnsnowlabs.nlp" %% "spark-nlp" % "3.4.4",
    "org.elasticsearch" %% "elasticsearch-spark-20" % "8.2.2",
    "org.json4s" %% "json4s-native" % "3.2.11",
)
